

public class Update {
	private Cache cache;
	private Object lock = new Object();

	public Update(Cache cache) {
		this.cache = cache;
	}

	public void Updater() {
		synchronized (lock) {
			cache.clear();
		}
	}
}