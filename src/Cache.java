

import java.util.Map;


public class Cache{
	
	private Cache(){}
	
	private Map<String, SystemEnumeration>items;
	private static Cache instance;
	private static Object token = new Object();
	
	public static Cache getInstance(){
		if(instance==null){
			synchronized(token){
				if(instance==null)
					instance = new Cache();
			}
		    }
			return instance;
	}

	public void setItems(Map<String, SystemEnumeration> items) {
		this.items = items;
	}
	
	public void get(String key){
		System.out.print(items.get(key));
	}
	public void clear(){
		items.clear();
	}
	public void clean(Map<String, SystemEnumeration> item){
		this.items = item;
	}
}